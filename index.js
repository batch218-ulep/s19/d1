// console.log("Hello World");

// What are conditional statements?
// Conditional statements allow us to control the flow of our program and it allow us to run statement/instruction based on the condition.

// if statement
// Executes a statement if a specified condition is true

/*
	Syntax:
		if(condition){
			code block/statements;
		}
*/

	let numA = 0;
	
	if(numA < 0){
		console.log("Hello");
	}

	//The codeblock inside the if statement will not work if it doens't satisfy the condition

	if(numA == 0){
		console.log("Hello");
	}

	// the result of the expression must result to "true" else it will not run the statement inside

	console.log(numA < 0); //false
	console.log(numA == 0); //true

	let city = "New York";
	if(city=="New York"){
		console.log("Welcome to the New York City");
	}

	// else if clause
	/*
		-executes a statement if previous conditions/s are false and if the specified condition is true
		-the "else if" clause is optional and can be added to capture additional conditions to change the flow f the program
	*/

	let numH = 0;

	if(numH<0){
		console.log("The number is positive!");
	}

	else if(numH>0){
		console.log("The number is positive!");
	}

	else if(numH==0){
		console.log("The value is zero");
	}


// alternative coode using else statement
	let num = 0;

	if(num<0){ //returned false
		console.log("The number is negative!");
	}

	else if(num>0){ //returned false
		console.log("The number is positive!");
	}

	else{
		console.log("The value is zero");
	}


	city = "Tokyo";

	if(city ==="New York"){
		console.log("Welcome to New York City");
	}
	else if(city == "Tokyo"){
		console.log("Welcome to Tokyo, Japan");
	}

	else{
		console.log("City is not included in the list");
	}

	/*
		else statement
		-executes a statement if all other condition are false
		-the else statement is optional and can be added to capture any other result to change the flow of the program
	*/

	let message = "No message";
		console.log(message);

		function determineTyphooneIntensity(windSpeed){

			if(windSpeed < 30){
				return "Not a typhoon yet."
			}
			else if(windSpeed <= 61){
				return "Tropical depression detected."
			}
			else if (windSpeed >= 62 && windSpeed <= 88){
				return "Tropical storm detected."
			}
			else if(windSpeed >= 89 && windSpeed <= 117){
				return "Severe Tropical storm deteceted";
			}
			else{
				return "Typhoon detected."
			}
		}

		message = determineTyphooneIntensity(30);
		console.log(message);

		//  console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
		if(message === "Severe Tropical storm deteceted"){
			console.warn(message);
		}

		let username = "admin";
		let password = "admin1234";

		if(username == "admin"){
			console.log("Correct username");
		}

		if(password == "admin1234"){
			console.log("Correct password");
		}

		else{
			console.log("Wrong username and password");
		}

		if(username == "admin" && password == "admin1234"){
			console.log("Successful Login");
		}

		else{
			console.log("Wrong username or password");
		}
		//[SECTION] Truthy and Falsy
		//- In javascript a "truthy" value that is considered true when encountered in a boolean context
		/*
			False
			0
			""
			null
			undefined // let number;
			Nan
		*/

		let isMarried = true;
		//truthy examples
		if(true){
			console.log("Truthy");
		}

		if(1){ //1 == true
			console.log("Truthy");
		}

		if([]){ //true
			console.log("Truthy");
		}

		//falsy example
		if(false){
			console.log("Falsy");
		}

		if(0){ //0 == false
			console.log("Falsy");
		}

		if(undefined){
			console.log("Falsy");
		}

		if(isMarried){
			console.log("Truthy");
		}

	//[SECTION] Conditonal (Ternary) Operator
/* 
	- The Conditional (Ternary) Operator takes in three operands:
	1. condition
    2. expression to execute if the condition is truthy
    3. expression to execute if the condition is falsy
*/

//Single statement excution
let ternaryResult = (1 < 18)? true : false; // ? --> if true, : --> if false
console.log("Result of Ternary Operator: " + ternaryResult);

//Multiple statement execution

let name;

function isOfLegalAge(){
	name = "John";
	return "You are of legal the age limit";
}

function isUnderAge(){
	name = "Jane";
	return "You are under the age limit";
}

let age = parseInt(prompt("What is your age?"));
//parseInt --> converts a string number into an Integer.
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in function: " + legalAge + ", " + name);

//[SECTION] Switch Statement


//Syntax
/*
switch (expression) {
		            case value:
		                statement;
		                break;
		            default:
		                statement;
		                break;
		        }
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case "monday":
		console.log("The color of the day is red");
		break;
	case "tuesday":
		console.log("The color of the day is orange");
		break;
	case "wednesday":
		console.log("The color of the day is yellow");
		break;
	case "thursday":
		console.log("The color of the day is green");
		break;
	case "friday":
		console.log("The color of the day is blue");
		break;
	case "saturday":
		console.log("The color of the day is indigo");
		break;
	case "sunday":
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day.");
		break;
}


//[SECTION] Try-Catch-Finally Statement

function showIntensityAlert(windSpeed){
	try{
		//attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch (error){
		// // error.message is used to access the information relating to an error object
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert.");
	}
}

showIntensityAlert(56);




